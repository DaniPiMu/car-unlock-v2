package com.project.carunlock.model

data class Tema(
    val name: String,
    val url:String
)
