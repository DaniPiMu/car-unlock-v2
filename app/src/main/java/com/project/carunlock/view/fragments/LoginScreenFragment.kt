package com.project.carunlock.view.fragments

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import com.project.carunlock.R
import com.project.carunlock.databinding.FragmentLoginScreenBinding

class LoginScreenFragment : Fragment() {

    private lateinit var binding: FragmentLoginScreenBinding
    private val db = FirebaseFirestore.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginScreenBinding.inflate(layoutInflater)

//        val prefs = context?.getSharedPreferences(getString(R.string.prefs_file),Context.MODE_PRIVATE)
//        prefs!!.edit().putString("email",binding.emailText.text.toString())
//        prefs.apply{}

        binding.googleButton.setOnClickListener {
            iniciarSesionGoogle()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.loginButton.setOnClickListener {
            loginUser()
        }
        binding.registrateButton.setOnClickListener {
            findNavController().navigate(R.id.action_loginScreenFragment_to_registerFragment)
        }
    }


    fun loginUser(){
        FirebaseAuth.getInstance().
        signInWithEmailAndPassword(binding.emailText.text.toString(), binding.passwordText.text.toString())
            .addOnCompleteListener {
                if(it.isSuccessful){
                    val emailLogged = it.result?.user?.email
                    goToHome(emailLogged!!)
                }
                else{
                    showError("Error al fer login")
                }
            }
    }

    private fun goToHome(emailLogged: String) {
        var registrado : Boolean= false
        db.collection("stats").document(emailLogged).get().addOnSuccessListener {
            registrado = true
        }
        if (!registrado) {
            createUserDB(emailLogged)
        }
        findNavController().navigate(R.id.action_loginScreenFragment_to_study)
    }

    private fun createUserDB(emailLogged: String) {
        db.collection("stats").document(emailLogged).set(
            hashMapOf(
                "erroresTema" to mapOf("t0" to 0, "t1" to 0,"t2" to 0,"t3" to 0,"t4" to 0,"t5" to 0,
                "t6" to 0,"t7" to 0, "t8" to 0,"t9" to 0, "t10" to 0, "t11" to 0,"t12" to 0, "t13" to 0),
                "preguntasAcertadas" to 0,
                "preguntasFalladas" to 0,
                "preguntasTotales" to 0,
                "testAprobados" to 0,
                "testSuspendidos" to 0,
                "testTotales" to 0,
                "tiempoTotal" to 0
            )
        )
    }

    private fun showError(error:String){
        Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
    }
    private fun iniciarSesionGoogle() {
        val googleConf = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(com.firebase.ui.auth.R.string.default_web_client_id))
            .requestEmail()
            .build()
        val googleClient = GoogleSignIn.getClient(requireContext(), googleConf)
        val signInIntent = googleClient.signInIntent
        googleClient.signOut()
        startActivityForResult(signInIntent, 100)
    }
    private fun showAlert() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Error")
        builder.setMessage("Se ha producido un error de inicio de sesion")
        builder.setPositiveButton("aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                    FirebaseAuth.getInstance().signInWithCredential(credential)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                val emailLogged = it.result?.user?.email
                                Toast.makeText(this.context, emailLogged, Toast.LENGTH_LONG).show()
                                if (emailLogged != null) {
                                    goToHome(emailLogged)
                                }
                            } else {
                                showAlert()
                            }
                        }
                }
            } catch (e: ApiException) {
                showAlert()
            }
        }
    }
}