package com.project.carunlock.view.fragments

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import coil.load
import com.project.carunlock.R
import com.project.carunlock.databinding.FragmentTestBinding
import com.project.carunlock.viewmodel.CarUnlockViewModel

class TestFragment : Fragment(), View.OnClickListener {

    private lateinit var binding : FragmentTestBinding
    private val model: CarUnlockViewModel by activityViewModels()
    private var num = 0
    private var isClicked = false

    //variables firebase
   var preguntasAcertadas = 0
   var preguntasFalladas = 0
   var preguntasTotales = 0
   var testAprobados = 0
   var testSuspendidos = 0
   var testTotales = 0
   var tiempoTotal = 0
    var erroresTema = mapOf("t0" to 0, "t1" to 0,"t2" to 0,"t3" to 0,"t4" to 0,"t5" to 0,
        "t6" to 0,"t7" to 0, "t8" to 0,"t9" to 0, "t10" to 0, "t11" to 0,"t12" to 0, "t13" to 0)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentTestBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.generateThirtyQuestions()

        model.questions.observe(viewLifecycleOwner, Observer {
            chargeQuestion()
        })

        binding.nextQuestion.setOnClickListener {
            if (num == 29) {
                model.clearData()
                view.let {
                    Navigation.findNavController(it).navigate(R.id.action_testFragment_to_study)
                }
            } else {
                isClicked = false
                resetColors()
                chargeQuestion()
            }
        }

        binding.testAnswer1Text.setOnClickListener(this)
        binding.testAnswer2Text.setOnClickListener(this)
        binding.testAnswer3Text.setOnClickListener(this)

    }



    private fun disableclick() {
        println("ENTROOOOO")
        binding.testAnswer1Text.isEnabled = false
        binding.testAnswer2Text.isEnabled = false
        binding.testAnswer3Text.isEnabled = false

    }

    fun chargeQuestion() {
        if (model.questions.value?.get(num)?.image.equals("")) {
            binding.testImage.setImageResource(R.drawable.no_image)
        } else {
            binding.testImage.load(model.questions.value?.get(num)?.image)
        }
        binding.testQuestion.text = model.questions.value?.get(num)?.title
        binding.testAnswer1Text.text = model.questions.value?.get(num)?.answer1
        binding.testAnswer1.setBackgroundColor(Color.TRANSPARENT)
        binding.testAnswer2Text.text = model.questions.value?.get(num)?.answer2
        binding.testAnswer2.setBackgroundColor(Color.TRANSPARENT)
        if (model.questions.value?.get(num)?.answer3.equals("")) {
            binding.testAnswer3.visibility = View.GONE
        } else {
            binding.testAnswer3.visibility = View.VISIBLE
            binding.testAnswer3Text.text = model.questions.value?.get(num)?.answer3
            binding.testAnswer3.setBackgroundColor(Color.TRANSPARENT)
        }
        var qc = "Pregunta "+(num+1)+" de 30"
        binding.testCounter.text = qc
        num++
    }

    override fun onClick(v: View?) {
        if (!isClicked){
            val textView: TextView = v as TextView
            if (textView.text.equals(model.questions.value?.get(num-1)?.answerC)) {
                textView.setBackgroundColor(Color.GREEN)
            } else {
                textView.setBackgroundColor(Color.RED)
            }
            isClicked = true
        }
    }

    private fun resetColors() {
        binding.testAnswer1Text.setBackgroundColor(R.color.white)
        binding.testAnswer1Text.setBackgroundColor(R.color.white)
        binding.testAnswer1Text.setBackgroundColor(R.color.white)
    }

}