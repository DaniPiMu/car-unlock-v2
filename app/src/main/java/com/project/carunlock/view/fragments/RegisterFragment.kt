package com.project.carunlock.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.project.carunlock.R
import com.project.carunlock.databinding.FragmentRegisterBinding


class RegisterFragment : Fragment() {

    private lateinit var binding: FragmentRegisterBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.registerButton.setOnClickListener {
            if (binding.passwordText.text.toString() == binding.repeatPasswordText.text.toString()){
                createUser()
            }else{
                binding.passwordText.setText("")
                binding.repeatPasswordText.setText("")
                Toast.makeText(requireContext(), "CONTRASEÑAS NO COINCIDEN !!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun createUser(){
        val email = binding.emailText.text.toString()
        val password = binding.passwordText.text.toString()
        FirebaseAuth.getInstance().
        createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if(it.isSuccessful){
                    val emailLogged = it.result?.user?.email
                    goToHome(emailLogged!!)
                }
                else{
                    showError("Error al registrar l'usuari")
                }
            }
    }

    private fun goToHome(emailLogged: String) {
        findNavController().navigate(R.id.action_registerFragment_to_loginScreenFragment)
    }

    private fun showError(error:String){
        Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
    }
}