package com.project.carunlock.view.fragments

import android.graphics.Color.rgb
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.fragment.app.Fragment
import com.project.carunlock.databinding.FragmentStatsBinding
import com.project.carunlock.view.generateRandomColor
import com.project.carunlock.view.generateRandomNumbers
import ir.mahozad.android.PieChart

class StatsFragment : Fragment() {

    private lateinit var binding: FragmentStatsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentStatsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sliceItems = listOf(
            PieChart.Slice(0.30f, rgb(120, 181, 0), rgb(149, 224, 0), legend = "Legend A"),
            PieChart.Slice(0.20f, rgb(204, 168, 0), rgb(249, 228, 0), legend = "Legend B"),
            PieChart.Slice(0.20f, rgb(0, 162, 216), rgb(31, 199, 255), legend = "Legend C"),
            PieChart.Slice(0.17f, rgb(255, 4, 4), rgb(255, 72, 86), legend = "Legend D"),
            PieChart.Slice(0.13f, rgb(160, 165, 170), rgb(175, 180, 185), legend = "Legend E")
        )

        //NO FUNCIONA PORQUE HE CAMBIADO EL MODULO DE LOS QUESOS,
        // ESTE SI CONSEGUIMOS QUE FUNCIONE VA DE LUJO, MAÑANA ME
        // LO SEGUIRE MIRANDO, CENTRAOS EN COMO HACER Y CALCULAR LAS ESTADISTICAS

//        setContent {
//            var slices by remember { mutableStateOf(sliceItems) }
//            MaterialTheme {
//                PieChartView(slices) {
//                    slices = generateRandomNumbers().mapIndexed { i, fraction ->
//                        PieChart.Slice(fraction, generateRandomColor(), legend = "Legend ${'A' + i}")
//                    }
//                }
//            }
//        }
    }

    @Composable
    fun PieChartView(sliceItems: List<PieChart.Slice>, onClick: () -> Unit) {
        AndroidView(
            modifier = Modifier
                .fillMaxSize()
                .clickable(onClick = onClick),
            factory = { context ->
                PieChart(context).apply {
                    slices = sliceItems
                    labelType = PieChart.LabelType.INSIDE
                    legendsTitle = "Legends"
                    isLegendEnabled = true
                    isAnimationEnabled = true
                    isLegendsPercentageEnabled = true
                    legendPosition = PieChart.LegendPosition.BOTTOM
                }
            },
            update = { chart ->
                // View's been inflated or state read in this block has been updated
                // Add logic here if necessary
                chart.slices = sliceItems
            }
        )
    }

}