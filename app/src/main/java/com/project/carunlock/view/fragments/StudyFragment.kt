package com.project.carunlock.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.project.carunlock.R
import com.project.carunlock.databinding.FragmentStudyBinding

class StudyFragment : Fragment() {

    private lateinit var binding: FragmentStudyBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentStudyBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.firstButton.setOnClickListener {
            view.let {
                Navigation.findNavController(it).navigate(R.id.action_study_to_testFragment)
            }
        }
        binding.secondButton.setOnClickListener {
            println("apretasion")
            view.let {
                println("clickasion")
                Navigation.findNavController(it).navigate(R.id.action_study_to_temarioFragment)
            }
        }

        println(this.id)

    }

}