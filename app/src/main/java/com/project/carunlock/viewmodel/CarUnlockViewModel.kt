package com.project.carunlock.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.project.carunlock.model.Question
import com.project.carunlock.model.RandomNumber
import com.project.carunlock.model.Tema
import java.util.*
import kotlin.coroutines.coroutineContext

class CarUnlockViewModel: ViewModel() {
    val temas = MutableLiveData<MutableList<Tema>>().apply {
        this.value = mutableListOf<Tema>(
            Tema("Tema 0","https://firebasestorage.googleapis.com/v0/b/carunlock-96030.appspot.com/o/Tema0.pdf?alt=media&token=09ecea36-4014-4d52-a907-c350f0ca9eea"),
            Tema("Tema 1","https://firebasestorage.googleapis.com/v0/b/carunlock-96030.appspot.com/o/temas%2FTema1.pdf?alt=media&token=100b877b-535a-4c8f-b3f4-93776aa3da25"),
            Tema("Tema 2",""),
            Tema("Tema 3",""),
            Tema("Tema 4",""),
            Tema("Tema 5",""),
            Tema("Tema 6",""),
            Tema("Tema 7",""),
            Tema("Tema 8",""),
            Tema("Tema 9",""),
            Tema("Tema 10",""),
            Tema("Tema 11",""),
            Tema("Tema 12",""),
            Tema("Tema 13","")
        )
    }

    var questions = MutableLiveData<MutableList<Question>>()
    var questionList = mutableListOf<Question>()
    val db = Firebase.firestore

    private fun fetchData(num : Int) {
        println("777777777777777777777777777777 "+num.toString())
        db.collection("questions")
            .whereEqualTo("id", num)
            .get()
            .addOnSuccessListener { documents ->
                println("Size of the document "+documents.size())
                for (document in documents) {
                    println(document.data.values.toString())
                    val myQuestion = document.toObject(Question::class.java)
                    println("++++++++++++++++++ "+myQuestion)
                    questionList.add(myQuestion)
                    println("454545454545455445454545454554545454545 "+questionList.size)
                    if (questionList.size == 30) {
                        questions.value = questionList
                    }
                }
            }

    }

    fun clearData() {
        questionList.clear()
    }

    fun generateThirtyQuestions() {
        val r = RandomNumber(1, 61)
        for (i in 1..30) {
            fetchData(r.nextInt())
        }
    }

    var selectedTema = MutableLiveData<Tema>()

    fun setTema(tema: Tema){
        selectedTema.value = tema
    }
}